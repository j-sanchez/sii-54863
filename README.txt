Instrucciones:

	- Movimiento del jugador 1:
		- Tecla 'w' para subir la raqueta.
		- Tecla 's' para bajar la raqueta.

	- Movimiento del jugador 2:
		- Tecla 'o' para subir la raqueta.
		- Tecla 'l' para bajar la raqueta.
