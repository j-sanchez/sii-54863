// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close(fd_fifo); // Cerrar descriptor de fifo
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	

	if(puntos1 == 3 || puntos2 == 3)
	{
		exit(0);
	}

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	
	int i;
	char buffer[64] = "\0"; // Buffer para lectura/escritura
	char str_puntos[32] = "\0"; // String para puntos (9 dig max de MAX_INT)
	
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	//if(jugador2.y1 < datos.raqueta.y1)
	//{
		// Implementar de alguna manera la funcion OnKeyboardDown(key, x, y);
	//}

	//OnKeyboardDown(key, x, y);

	datos.esfera.radio = esfera.radio;
	datos.raqueta1.y1 = jugador2.y1;
	datos.raqueta1.y2 = jugador2.y2;

	jugador2.y1 = esfera.centro.y + 1;
	jugador2.y2 = esfera.centro.y - 1;

	if(jugador1.Rebota(esfera))
	{
		esfera.radio-=0.02f; // Reduccion tamanio esfera al contacto con jugador 1
		
		if(esfera.radio<0.2f)
		{
			esfera.radio=0.2f; // Valor minimo de esfera
		}
	}
	if(jugador2.Rebota(esfera))
	{
		esfera.radio-=0.02f; // Reduccion tamanio esfera al contacto con jugador 2
		
		if(esfera.radio<0.2f)
		{
			esfera.radio=0.2f; // Valor minimo de esfera
		}
	}
	
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=0.1f+0.1f*rand()/(float)RAND_MAX;
		esfera.velocidad.y=0.1f+0.1f*rand()/(float)RAND_MAX;
		
		esfera.radio=0.5f; // Reinica tamanio esfera
		
		puntos2++;
		
		strcat(buffer, "Jugador2"); // Pone "Jugador 2" en buffer 
		strcat(buffer, "#"); // Pone "#" en buffer para separar nombre de puntos
		sprintf(str_puntos, "%d", puntos2); // Cambia de int a string los puntos
		strcat(buffer, str_puntos); // Aniade a buffer los puntos
		
		write(fd_fifo, buffer, sizeof(buffer)); // Escriebe en fifo el buffer
		//printf("%s", buffer); // Imprime por pantalla lo que deberia escribir en fifo para comprobacion
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-0.1f-0.1f*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-0.1f-0.1f*rand()/(float)RAND_MAX;	
		
		esfera.radio=0.5f;
		
		puntos1++;
		
		// Igual explicacion que en funcion {void CMundo::OnTimer(int value) -> if(fondo_izq.Rebota(esfera))}
		
		strcat(buffer, "Jugador1");
		strcat(buffer, "#");
		sprintf(str_puntos, "%d", puntos1);		
		strcat(buffer, str_puntos);
		
		write(fd_fifo, buffer, sizeof(buffer));
		//printf("%s", buffer);	
	}

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-0.25f;break;
	case 'w':jugador1.velocidad.y=0.25f;break;
	case 'l':jugador2.velocidad.y=-0.25f;break;
	case 'o':jugador2.velocidad.y=0.25f;break;

	}
}

void CMundo::Init()
{
	fd_fifo = open("my_fifo", O_WRONLY); // Abre fifo en modo solo escrituta
	if(!fd_fifo)
	{
		printf("Error al abrir fifo"); // Salir si error al abrir
		exit(-1);
	}

	fd_fichero = open("my_fichero", 0666);
	if(fd_fichero == 0) 
	{
	 	printf("Error al crear el fichero\n");
	 	exit(-1);
	}
	else
	{
		mmap(ptr_datos, sizeof(DatosMemCompartida), PROT_READ | PROT_WRITE, MAP_SHARED, fd_fichero, 0);
	}
	
	close(fd_fichero);
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
