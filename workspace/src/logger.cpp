#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char* argv[])
{
	char buffer[64] = "\0"; // Buffer para lectura/escritura

	if(mkfifo("my_fifo", 0666) == -1) // Crear fifo en modo solo lectura
	{
		printf("Error en la creacion del fifo"); // Salir si error en creacion de fifo
		return -1;
	}
	else
	{
		int fd_fifo = open("my_fifo", O_RDONLY); // Abrir fifo en modo solo lectura
		if(!fd_fifo)
		{
			printf("Error al abrir fifo"); // Salir si error en apertura de fifo
			return -1;
		}	
		else
		{
			while(read(fd_fifo, buffer, sizeof(buffer))) // Bucle de lectura de fifo
			{
				char name[32] = "\0"; // String nombre
				char puntos[32] = "\0"; // String puntos
				
				int i = 0;
				
				while(buffer[i] != '#') // Todo lo que va antes del # es el nombre
				{
					char c = buffer[i];
					strcat(name, &c);
					i++;
				}
				
				i++;
				
				while(buffer[i] != '\0') // Todo lo que va despues del # hasta el final son puntos
				{
					char c = buffer[i];
					strcat(puntos, &c);
					i++;
				}
				
				printf("%s marca 1 punto, lleva un total de %d puntos.\n", name, atoi(puntos)); // Imprime para comprobacion
				
			}
		
			close(fd_fifo); // Cerrar fifo
			unlink("my_fifo"); // Destruir fifo
		}
	}
}
