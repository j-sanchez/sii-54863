#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>


int main(int argc, char* argv[])
{
	DatosMemCompartida* datos;
	
	int fd_fichero = open("my_fichero", O_RDONLY);
	if(fd_fichero == 0) 
	{
	 	printf("Error al abrir el fichero en modo lectura\n");
	 	exit(-1);
	}
	else
	{
		mmap(datos, sizeof(DatosMemCompartida), PROT_READ, MAP_SHARED, fd_fichero, 0);
	}
	
	close(fd_fichero);
	
	while(1)
	{
		usleep(25000);
		if(datos->esfera.centro.y > datos->raqueta1.y1)
		{
			// Mandar un 1 para ir hacia arriba
		}
		else if(datos->esfera.centro.y < datos->raqueta1.y1)
		{
			// Mandar un -1 para ir hacia abajo
		}
		else
		{
			// Mandar un 0 para no hacer nada
		}
	}
	
	munmap(&datos, sizeof(DatosMemCompartida));
	
	return 0;
}
